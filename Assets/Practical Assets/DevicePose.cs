﻿/*
 * Script based on code from:
 * https://forum.unity3d.com/threads/sharing-gyroscope-camera-script-ios-tested.241825/
 */
using UnityEngine;
using System.Collections;

public class DevicePose : MonoBehaviour 
{
	void Start()
	{
		Input.gyro.enabled = true;
	}

	void Update() 
	{
		transform.rotation = Input.gyro.attitude;
		transform.Rotate(0.0f, 0.0f, 180.0f, Space.Self); 
		transform.Rotate(90.0f, 180.0f, 0.0f, Space.World);
	}
}
